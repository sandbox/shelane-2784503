<?php

/**
 * @file
 * Definition of mymodule_handler_handlername.
 */

/**
 * Description of what my handler does.
 */
class views_addons_handler_current_date extends views_addons_simple_field_handler {
  public $field_alias = "current_date";
  
  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
  }

  function get_value($values, $field = NULL) {
    return date($this->options['format']);
  }

  /**
   * Default options form.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['format'] = array('default' => 'm/d/y');

    return $options;
  }

  /**
   * Creates the form item for the options added.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['format'] = array(
      '#type' => 'textfield',
      '#title' => t('Date Format'),
      '#default_value' => $this->options['format'],
      '#description' => t('Formatted date, using guidelines from <a href="http://php.net/manual/en/function.date.php">this page</a>'),
      '#weight' => -10,
    );
  }
}
