<?php

/**
 * @file
 * Defines since_emergency_started filter handler.
 */

class views_addons_since_emergency_started_filter_handler extends views_handler_filter {
  private $startTime;

  function construct() {
    parent::construct();
    $this->startTime = 0;
  }

  /**
   * Determine which flags the current user has access to.
   */
  function pre_query() {
    $this->startTime = strtotime(variable_get("start_date"));
  }

  /**
   * Modifies the query to support this filter.
   */
  function query() {
    $this->query->add_where($this->options["group"], "node.created", (int)$this->startTime, ">");
  }
}
