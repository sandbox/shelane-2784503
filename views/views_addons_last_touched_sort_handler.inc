<?php

/**
 * @file
 * Defines the last_touched sort handler.
 */

class views_addons_last_touched_sort_handler extends views_handler_sort {
  /**
   * Add appropriate joins and ORDER BY clauses to the query.
   */
  function query(){
    $sub_query = db_select("node", "node");
    $sub_query->addField("node", "nid");
    $sub_query->addJoin("LEFT", "workflow_node", "workflow", "workflow.nid = node.nid");
    $sub_query->addJoin("LEFT", "flagging", "flag", "flag.entity_id = node.nid");
    $sub_query->addExpression("MAX(GREATEST(node.changed, IFNULL(flag.timestamp, 0), IFNULL(workflow.stamp, 0)))", "touched_at");
    $sub_query->groupBy("nid");

    $join = new views_join();
    $join->definition = array(
      'table formula' => $sub_query,
      'left_field' => 'nid',
      'field' => 'nid',
      'left_table' => 'node',
    );
    $join->left_table = 'node';
    $join->field = 'nid';
    $join->left_field = 'nid';
    $join->type = "LEFT";

    // Add workflow_node to the table.
    $table_alias = $this->query->ensure_table("sub_query", "node", $join);
    $this->query->add_field($table_alias, "touched_at", "touched_at");

    // Add the order by clause.
    // GREATEST selects a single column, IFNULL is mysql's NVL.
    $this->query->add_orderby(NULL, "touched_at");
  }
}
