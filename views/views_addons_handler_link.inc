<?php

/**
 * @file
 * Definition of mymodule_handler_handlername.
 */

/**
 * Description of what my handler does.
 */
class views_addons_handler_link extends views_addons_simple_field_handler {
  public $field_alias = "system_link";
  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
  }

  /**
   * Loads the actual value we want to display.
   */
  function get_value($values, $field = NULL) {
    return url($this->options["url_path"], array("absolute"=>TRUE));
  }

  /**
   * Default options form.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['url_path'] = array('default' => '<front>');

    return $options;
  }

  /**
   * Creates the form item for the options added.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['url_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to link'),
      '#default_value' => $this->options['url_path'],
      '#description' => t('Creates a URL from the specified path.'),
      '#weight' => -10,
    );
  }
}
