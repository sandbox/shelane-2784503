<?php
/**
 * @file
 * Views definitions for views addons.
 */

/**
 * Implements hook_views_data().
 */
function views_addons_views_data() {
  $data = array();
  $data['node']['current_date'] = array(
    'title' => t('Current Date'),
    'help' => t('Adds the current date, in whatever format you choose.'),
    'field' => array(
      'handler' => 'views_addons_handler_current_date',
    ),
  );
  $data['node']['system_link'] = array(
    'title' => t('Relative URL'),
    'help' => t('Creates a URL to a path on this server.'),
    'field' => array(
      'handler' => 'views_addons_handler_link',
    ),
  );
  $data["node"]["last_touched"] = array(
    "title" => t("Last Interacted With"),
    "help" => t("The timestamp at which the node was last flagged, workflow modified, or updated."),
    "sort" => array(
      "handler" => "views_addons_last_touched_sort_handler",
    ),
  );
  $data["node"]["can_flag"] = array(
    "title" => t("Node is Flaggable"),
    "help" => t("Checks if there are any flags the current user can apply to the entity."),
    "filter" => array(
      "handler" => "views_addons_user_can_flag_filter_handler",
    ),
  );
  $data["node"]["since_emergency"] = array(
    "title" => t("Since Emergency Began"),
    "help" => t("Checks if the node was created after the current emergency began."),
    "filter" => array(
      "handler" => "views_addons_since_emergency_started_filter_handler",
    ),
  );
  return $data;
}
