<?php

/**
 * @file
* Definition of mymodule_handler_handlername.
*/

/**
 * Description of what my handler does.
*/
class views_addons_simple_field_handler extends views_handler_field {
  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
  }

  /**
   * Loads additional fields into the query.
   */
  function query() {
  }

  /**
   * Add this field onto the result, in post_execute.
   */
  function post_execute(&$result) {
    $key = $this->field_alias;
    $result[0]->{$key} = $this->get_value($result[0]);
  }
}
