<?php

/**
 * @file
 * Defines user_can_flag filter handler.
 */

class views_addons_user_can_flag_filter_handler extends views_handler_filter {
  private $flags;

  function construct() {
    parent::construct();
    $this->flags = array();
  }

  /**
   * Determine which flags the current user has access to.
   */
  function pre_query() {

    // For now, we're only concerned about nodes.
    $all_flags = flag_get_flags("node");

    $this->flags = array();
    foreach ($all_flags as $flag_name => $flag) {
      if (user_access("flag $flag_name")) {
        $this->flags[] = $flag->fid;
      }
    }

  }

  function query() {

    // Join a subquery that checks the number of flags per node.
    if (count($this->flags)) {

      $sub_query = db_select('flagging', 'flagging');
      $sub_query->addField('flagging', 'entity_id', 'nid');
      $sub_query->addExpression('COUNT(entity_id)', "flags");
      $flag_subbing = array();
      for ($i = 0; $i < count($this->flags); $i++) {
        $flag_subbing[":flag_$i"] = $this->flags[$i];
      }
      $flag_keys = implode(", ", array_keys($flag_subbing));
      // $flag_keys is entirely constructed from programmatic data, no user input, no sanitization necessary.
      $sub_query->where("fid in ($flag_keys)", $flag_subbing);
      $sub_query->where("entity_type = 'node'");
      $sub_query->groupBy("nid");

      $join = new views_join();
      $join->definition = array('table formula' => $sub_query, 'left_field' => 'nid', 'field' => 'nid', 'left_table' => 'node');
      $join->left_table = 'node';
      $join->field = 'nid';
      $join->left_field = 'nid';
      $join->type = 'LEFT OUTER';

      // This is safe. Can only be an integer.
      $join->extra = 'flagging.flags < ' . count($this->flags);

      $table = $this->query->ensure_table("flagging", "node", $join);
    }
  }
}
